<?php include("config.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="msvalidate.01" content="29049D05C590C1160FAC4C07498B5EF1" />
  <title>Kelowna Infiniti - New Infiniti Vehicles in Kelowna, British Columbia</title>
  <meta name="description" content="New &amp; Pre-Owned Infiniti vehicles in Kelowna, British Columbia at the Kelowna Infiniti dealership in Canada. We offer vehicle servicing, auto parts, build and price, and online financing." />
  <meta name="keywords" content="Kelowna British Columbia dealer, infiniti, kelowna, british columbia, new used vehicles, pre-owned cars" />
  <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
  
  <script type="text/javascript" src="//use.typekit.net/tzr4nqq.js"></script>
  <script type="text/javascript">try{Typekit.load();}catch(e){}</script>

  <!--CSS-->
  <link rel="stylesheet" href="/css/reset.css" type="text/css" />
  <link rel="stylesheet" href="/css/960_12_col.css" type="text/css" />
  <link rel="stylesheet" href="/css/styles.css" type="text/css" />
  <link rel="stylesheet" href="/css/leads.css" type="text/css" />
  <link rel="stylesheet" href="includes/jquery-ui.theme.min.css" type="text/css" />
  <link rel="stylesheet" href="includes/jquery-ui.structure.min.css" type="text/css" />

  <!-- JS -->
  <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
  <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  <script src="includes/jquery-ui.min.js"></script>

  <script>
  $(function() {
    $( ".date-picker" ).datepicker({
      showOn: "button",
      buttonImage: "/service/img/date-picker.png",
      buttonImageOnly: true,
      buttonText: "Select date"
    });
  });
  </script>

  <!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>

<body<?php echo $extra_body_stuff; ?>>

<div id="header">
  <div class="container_12">
    <div class="grid_2 infiniti-logo">
      <img src="/images/infiniti-logo.jpg" alt="Infiniti Logo"/>
    </div>
    <div class="grid_4 m10">
      <span class="logo"><a href="/">Kelowna Infiniti</a></span>
      <div class="partof"><a href="http://www.sentes.com/" title="Sentes Automative Group" target="_blank">A Sentes Dealership</a></div>      
    </div>
    <div class="grid_5 header-contact-info">
      <b>Toll-Free:</b> 1-800-558-3377 | <b>Sales:</b> (250) 448-2306<br/>
      2570 Enterprise Way Kelowna, BC V1X 7X5
    </div>
  </div>
</div>

  <div id="navigation-bar">
    <div class="container_12">
      <div class="grid_12">
        <nav>
          <ul class="nav sfmenu" id="nav">
            <li><a href="http://www.kelownainfinitinissan.com/dcm/search-new-vehicles?make=Infiniti">New Infiniti Vehicles</a></li>
            <li><a href="http://www.kelownainfinitinissan.com/dcm/search-used-vehicles">Used Vehicles</a></li>
            <li><a href="/service/">Service</a></li>
            <li><a href="http://www.kelownainfinitinissan.com/dcm/specials/parts">Parts</a></li>
            <li><a href="http://www.kelownainfinitinissan.com/dcm/contact">About</a></li>       
          </ul>
        </nav>
      </div>
    </div>
  </div>