<?

/*
		#############################################
		##                                         ##
		##       FORM 2 EMAIL SCRIPT v1.1.2        ##
		##          by Daniel Williams             ##
		##                                         ##
		##      http://www.form2emailscript.com    ##
		##                                         ##
		#############################################

Copyright 2008 Internet Services Group Limited. All Rights Reserved.

This program may be used and modified free of charge by anyone, so long as this
copyright notice and the header above remain intact.  By using this program you agree
to indemnify Internet Services Group Limited from any liability.

Selling the code for this program without prior written consent is expressly forbidden.
Obtain permission before redistributing this program over the Internet or in any other
medium.  In all cases copyright and header must remain intact.

This is a FREE script so support is limited and a reply is *not* guaranteed.
Send bug reports, suggestions, feedback and translations to support@form2emailscript.com


#################################################################################
#										#										#										#
#	FREE WEBSITE STATISTICS at http://www.clearviewstats.com		#
#										#										#										#
#	Invisible Tracker! View visitors by Country, Region, City, and ISP.	#
#	View search phrases your visitors use on Google, MSN, and Yahoo.	#
#	View your site's activity in real time, plus much much more.		#
#										#										#										#
#################################################################################

################################
# FEATURES						
################################

- Email the results of any form just by posting to this script.
- Prevent spam abuse with a list of allowed Domains (in script or in file.)
- Check required fields have been completed before allowing send.
- Validate's user's email address.
- Log form data to a CSV file.
- Ban words and phrases.
- Ban users by IP address.
- Redirect to a "thank you" page upon successfully sending.
- Configure in your own language.

################################
# REQUIREMENTS					
################################

- Script must run on server supporting PHP.

################################
# EXAMPLE HTML CODE				
################################

<form method="post" action="form2email.php">
<input type="hidden" name="to" value="me@mydomain">
<input type="hidden" name="subject" value="Website Feedback">
<input type="hidden" name="name_reqd">
<input type="hidden" name="email_reqd">
<input type="hidden" name="message_reqd">
<p>Name: <input type="text" name="name">
<p>Email: <input type="text" name="email">
<p><textarea cols="50" rows="5" name="message"></textarea>
<p><input type="submit" value="Send Feedback">
</form>

################################
# QUICK REFERENCE FIELDS		
################################

-------------------------------------------------------------------------------
	Description	|	Input Name	| 	Input Required	
-------------------------------------------------------------------------------
	To		|	to		|	n/a				
	Name		|	name		|	name_reqd		
	Email		|	email		|	email_reqd		
	Subject		|	subject		|	subject_reqd	
	Redirect	|	redirect	|	n/a				
	Header		|	header		|	n/a
	Footer		|	footer		|	n/a
-------------------------------------------------------------------------------

################################
# CONFIRGURABLE FIELDS			
################################
*/


$recipient = (empty($_POST['recipient'])) ? 'default' : $_POST['recipient']; 
$email_addresses = array('default' => 'nilskelly@sentes.com, mikeoughtred@sentes.com, saleskelownanissan@sentes.com, scottrobb@sentes.com, dionedwards@sentes.com, karahennen@sentes.com'); 

if(!array_key_exists($recipient, $email_addresses)){
    $recipient = $email_addresses['default']; 
} else { 
    $recipient = $email_addresses[$recipient]; 
}  

// Set a to field if this script will always post to the same email address (also protects from spammers.)
// e.g. $to = "you@domain.com";

$to = $recipient; // see individual forms

// Add a list of Domains that can post to this script, separated by comma (include Domains with and without www.)
// e.g. $referrers = "www.domain.com, domain.com";

$referrers = "http://customers.strathcom2.com, customers.strathcom2.com, www.customers.strathcom2.com, http://kelownainfiniti.com, www.kelownainfiniti.com, kelownainfiniti.com";

// Alternatively, provide an external file with a list of allowed Domains, each separated on a new line (include Domains with and without www.)
// e.g. $referrers_file = "/home/username/httpdocs/referrers.txt"; or
// e.g. $referrers_file = "referrers.txt";

$referrers_file = "";

// You can specify a redirect upon successful posting either here or in the form as a hidden field.
// e.g. $redirect = "http://www.yourdomain.com/thanks.html";

$redirect = "../thankyou.php";

// If you want form posts recorded to a CSV file, provide an external file.
// e.g. $csv_file = "/home/username/httpdocs/form_data.csv"; or
// e.g. $csv_file = "form_data.csv";

$csv_file = "";

// You can stop your form being submitted when specific words and phrases are in any of the fields.
// Separate each phrase with commas. Phrase checking is case-insensitive.
// e.g. $banned_phrases = "University Diploma, Add Inches";

$banned_phrases = "";

// Alternatively, provide an external file with a list of banned phrases, each separated on a new line.
// e.g. $csv_file = "/home/username/httpdocs/banned_phrases.txt"; or
// e.g. $csv_file = "banned_phrases.txt";

$banned_phrases_file = "";

// You can stop your form being submitted from specific IP addresses.
// Separate each phrase with commas. 
// e.g. $banned_phrases = "123.456.789.123, 987.654.321.123";

$banned_ips = "";

// Alternatively, provide an external file with a list of banned IPs, each separated on a new line.
// e.g. $csv_file = "/home/username/httpdocs/banned_ips.txt"; or
// e.g. $csv_file = "banned_ips.txt";

$banned_ips_file = "";

// Set your preferred language (translations required.)

$lang = "en-us";

// If you have a header file for the error page, include the full path.
// e.g. $csv_file = "/home/username/httpdocs/header.html"; or
// e.g. $csv_file = "header.html";

$header_file = "";

// Alternatively you can specify a header file in the form. See further down for instructions.

// If you have a footer file for the error page, include the full path.
// e.g. $csv_file = "/home/username/httpdocs/footer.html"; or
// e.g. $csv_file = "footer.html";

$footer_file = "";

// Alternatively you can specify a footer file in the form. See further down for instructions.

/*

################################
# HELP: EMAIL TO				
################################

$to

You can fix an email address that the form results will always be posted to.
If you leave this field blank then you must have a field in your form with the name "to".
e.g. <input type="hidden" name="to">

################################
# HELP: REFERRERS				
################################

$referrers

Where your "to" variable is part of your form you should include a list of referrers 
to prevent spammers from using this file to send spam. Separate each referrer by a comma.
If your site is accessible without a www in front of the URL you'll need to add this address too.

$referrers_file

To automate the administration of referrers this script can call a refferer file.
The file must have each Domain that can use this script on a separate line.
If your site is accessible without a www in front of the URL you'll need to add this address too.
Include the full path to the file.

################################
# HELP: OUTPUT TO CSV			
################################

$csv_file

// If you want form posts recorded to a CSV file, provide an external file.
// To write to this file you must create and upload a blank file and CHMOD to 777
// or CHMOD 777 the folder the file will be created in.

################################
# SUBMITTING FORM				
################################

Set your form to post to this script and the script will do the rest!
e.g. <form method="post" action="form2email.php">

################################
# EMAIL FROM					
################################

To make the email come from the person posting it use field names "name" for their 
name and "email" for their email. This will then send the mail from "Name <Email>".
e.g. <input type="text" name="name">
e.g. <input type="text" name="email">

################################
# EMAIL SUBJECT					
################################

The subject of the email will come from your form. This could be from a hidden field 
a select list, or text field. name the field "subject".
e.g. <input type="hidden" name="subject" value="Site Feedback">

################################
# EMAIL BODY					
################################

The email body will be made up of the field names in the form and the data entered by the user.

################################
# REDIRECT						
################################

This is the page the user will be redirected to once the form has been submitted.
The field should be called "redirect".
e.g. <input type="hidden" name="redirect" value="http://www.yourdomain.com/thanks.html">

################################
# REQUIRED FIELDS				
################################

You can check required fields by adding hidden input fields under the <form> tag. Simply add "_reqd" to 
the end of the field name you need entered.

We recommend adding these fields to your form.

<input type="hidden" name="name_reqd">
<input type="hidden" name="email_reqd">

################################
# HEADER/FOOTER					
################################

As well as being able to specify a header and footer file in this script you can let different forms 
using this script include their own files. To do this, use the hidden fields below:

<input type="hidden" name="header" value="header.html">
<input type="hidden" name="header" value="footer.html">

You may need to use the full file path if this script is running from a different Domain, but we don't 
recommend this, for security reasons.

*/

################################
# TRANSLATIONS					
################################

if ($lang == "en-us") {
	$langErrorSetTo = "Please set an address to send the mail to.";
	$langErrorNoPermission = "The website you posted your form from does not have permission to use this script. Please add this site's Domain to the scripts allowed referrers.";
	$langCheckReqd = " ";
	$langBannedPhrase = "Sorry but the data you have submitted contains words or phrases that are not allowed.";
	$langIPBanned = "Sorry but you're unable submit the form at this time.";
	$langEmailNotOk = "</li><h4>Your email address is invalid, please enter a valid email address.</h4>";
	$langCsvLogged = "This data has been logged to a CSV file.";
	$langCsvFailed = "WARNING: Could not log to CSV file. Please CHMOD 777 the home folder of form2email.php.";
	$langForm2EmailResults = "Form2Email Results";
	$langFormSubmitted = "Thanks, your form was submitted.";
	$langFormError = "Form2Email Error";
	$langFormResults = "Form2Email Results";
	$langSubmitError = "<h3>Please fill out the required fields:</h3>";
	$langGoBack = "Go Back";
	$langErrorNoReferFile = "The referrer file specified does not exist.";
	$langErrorNoPhrasesFile = "The banned phrases file does not exist.";
	$langErrorNoIPsFile = "The banned IPs file does not exist.";
	$errorEmailSubject = "Form2Email File Errors";
}

################################
# DO NOT MODIFY CODE BELOW		
################################

# FUNCTIONS

// This function checks the email address for invalid chars.
function emailOK($str) {
	$badChars = "[ ]+| |\+|=|[|]|{|}|`|\(|\)|,|;|:|!|<|>|%|\*|/|'|\"|\?|#|\\$|\\&|\\^|www[.]";
	return (eregi($badChars,$str));
}

# CREATE TO FIELD

// Check if the to field has been set in this script.
if ($to == "") {
	// it hasn't, so set it as a posted field.
	$to = $_POST["to"];
}

// Check if a to value exists, otherwise generate an error.
if ($to == "")
	$error[] = $langErrorSetTo;

# CHECK IN-SCRIPT REFERRERS

// Check if there are referrers set.
if ($referrers != "") {
	// Read the refferers into an array.
	$referrersAr2 = explode(",",$referrers);
	$numLines = sizeof($referrersAr2);
	for ($i=0;$i<$numLines;$i++) {
		$referrersAr[] = trim($referrersAr2[$i]);
	}
}

# CHECK EXTERNAL FILE REFERRERS

// Check if a file path has been set.
if ($referrers_file != "") {
	// Check if the file exists.
	if (file_exists($referrers_file)) {
	
		// Check the file has content and then open.
		if (filesize($referrers_file) > 0) {
			$fd = fopen( $referrers_file, "r" );
			$referrer_content = fread( $fd, filesize( $referrers_file ) );
			fclose( $fd );
		}
		
		// Read lines into an array.
		$referrersAr2 = explode("\n",$referrer_content);
		$numLines = sizeof($referrersAr2);
		for ($i=0;$i<$numLines;$i++) {
			$referrersAr[] = trim($referrersAr2[$i]);
		}
	
	} else {
		// Create a file error to be emailed.
		$errorEmail[] = $langErrorNoReferFile;
	}
}

# CHECK DOMAIN AGAINST ALLOWED REFERRERS

// Check if the Domain of the submitting site is in our referrers array.
$host = $_SERVER["HTTP_HOST"];

if (is_array($referrersAr)) {
	if (!in_array($host, $referrersAr)) {

		// The host is not in the array, so decline sending.
		$error[] = $langErrorNoPermission;
	}
}

# CHECK FOR REQUIRED FIELDS

// Check if there are any required fields.
$reqdAr = array();
foreach($_POST as $key => $value) {
	// Check if the field name contains "_reqd".
	if (ereg("_reqd",$key,$regs)) {
		// Replace the _reqd and put in a check array.
		$key2 = str_replace("_reqd","",$key);
		$reqdAr[] = $key2;
	} else {
		if (in_array($key,$reqdAr) && $value == "")
			$error[] = $langCheckReqd."<b>".$key."</b>";
	}
}

# CHECK BANNED WORDS AND PHRASES

// Check if a file path has been set.
if ($banned_phrases_file != "") {
	// Check if the file exists.
	if (file_exists($banned_phrases_file)) {
	
		// Check the file has content and then open.
		if (filesize($banned_phrases_file) > 0) {
			$fd = fopen( $banned_phrases_file, "r" );
			$banned_phrase_content = fread( $fd, filesize( $banned_phrases_file ) );
			fclose( $fd );
		}
		
		// Read lines into an array.
		$banned_phrasesAr2 = explode("\n",$banned_phrase_content);
		$numLines = sizeof($banned_phrasesAr2);
		for ($i=0;$i<$numLines;$i++) {
			$banned_phrasesAr[] = trim($banned_phrasesAr2[$i]);
		}
	
	} else {
		// Create a file error to be emailed.
		$errorEmail[] = $langErrorNoPhrasesFile;
	}
}

// Check if the fields contain any banned phrases.
if ($banned_phrases != "") {

	// Read the phrases into an array.
	$banned_phrasesAr2 = explode(",",$banned_phrases);
	$numLines = sizeof($banned_phrasesAr2);
	for ($i=0;$i<$numLines;$i++) {
		$banned_phrasesAr[] = trim($banned_phrasesAr2[$i]);
	}
}

// Check phrases against submitted content.
if ($banned_phrases != "" || $banned_phrases_file != "") {
	foreach($_POST as $key => $value) {
		if ($key != "to" && $key != "redirect" && $key != "header" && $key != "footer" && !ereg("_reqd",$key,$regs)) {

			// Check each item in the array.
			$numPhrases = sizeof($banned_phrasesAr);
			for ($i=0;$i<$numPhrases;$i++) {

				if (eregi($banned_phrasesAr[$i],$value,$regs)) {
				
					// An item has been detected. Generate an error.
					$error[] = $langBannedPhrase;
				
					// Escalate the count to stop checking.
					$i = $numPhrases;
				}
			}
		}
	}
}

# CHECK BANNED IPS

// Get the user's IP.
$user_ip = $_SERVER['REMOTE_ADDR'];

// Check if a file path has been set.
if ($banned_ips_file != "") {
	// Check if the file exists.
	if (file_exists($banned_ips_file)) {
	
		// Check the file has content and then open.
		if (filesize($banned_ips_file) > 0) {
			$fd = fopen( $banned_ips_file, "r" );
			$banned_ips_content = fread( $fd, filesize( $banned_ips_file ) );
			fclose( $fd );
		}
		
		// Check if the user's IP is in the content.
		if (ereg($user_ip,$banned_ips_content,$regs))
			$error[] = $langIPBanned;
	
	} else {
	// Create a file error to be emailed.
		$errorEmail[] = $langErrorNoIPsFile;
	}
}

// Check if the fields contain any banned IPs.
if ($banned_ips != "") {

	if (ereg($user_ip,$banned_ips,$regs))
		$error[] = $langIPBanned;
}

# CHECK IF EMAIL ADDRESS IS VALID

foreach($_POST as $key => $value) {
	if ($key == "email") {
		if (!(eregi("([a-z0-9_\.-])+@([a-z0-9_\.-])+\.([a-z0-9_\.-])+",$_POST["email"])) || emailOK($_POST["email"]))
			$error[] = $langEmailNotOk;
	}
}

# PROCESS FORM

// Check if there are any errors.
if ($error == "") {

	# LOG DATA TO CSV

	// Check if data needs to be logged to CSV
	if ($csv_file != "") {
	
		// First check if this file exists
		if (!file_exists($csv_file)) {

			// Create titles
			foreach($_POST as $key => $value) {
				if ($key != "to" && $key != "redirect" && $key != "header" && $key != "footer" && !ereg("_reqd",$key,$regs)) {
					$key = str_replace("\"","\"\"",$key);
					$csvTitles .= "\"".$key."\",";
				}
			}
		}

		// Create line of data
		foreach($_POST as $key => $value) {
			if ($key != "to" && $key != "redirect" && $key != "header" && $key != "footer" && !ereg("_reqd",$key,$regs)) {
				$value = str_replace("\"","\"\"",$value);
				$csvLine .= "\"".$value."\",";
			}
		}
		
		// Write to the file
		if ($csvTitles != "")
			$fileContent = $csvTitles."\n";
	
		$fileContent .= $csvLine."\n";

		$cartFile = @fopen($csv_file,"a");
		@fputs($cartFile,$fileContent);
		@fclose($cartFile);

	}
	
	# CREATE SUBJECT

	// Set the subject if none exists.
	if ($_POST["subject"] == "")
		$subject = $langFormResults;
	else
		$subject = $_POST["subject"];
		
	// Set the FROM name (1 name input field)
	if ($_POST["name"] != "" && $_POST["email"] != "") {
		$from = $_POST["name"]." <".$_POST["email"].">";

    // Set the FROM name (1st and last name combined)	
	} else if ($_POST["FIRST_NAME"] != "" && $_POST["LAST_NAME"] != "" && $_POST["email"] != "") {
		$from = $_POST["FIRST_NAME"]. " ". $_POST["LAST_NAME"]." <".$_POST["email"].">";
	}
    	
	# CREATE EMAIL BODY
	
	$body = "";
	
	foreach($_POST as $key => $value) {
		if ($key != "to" && $key != "subject" && $key != "redirect" && $key != "header" && $key != "footer" && $key != "submit_x" && $key != "submit_y" && $key != "submit" && !ereg("_reqd",$key,$regs))
			$body .= $key.": ".$value."\n";
	}
	
	// Check if a message about the CSV file needs to be attached.
	if ($csv_file != "") {
		if (file_exists($csv_file))
			$body .= $langCsvLogged."\n";
		else
			$body .= $langCsvFailed."\n";
	}
	
	// Append user details.
	$body .= "Posted From: www.pentitionhyundai.com/dcm/trade-appraisal"."\n";
	$body .= "User Host: ".gethostbyaddr($_SERVER['REMOTE_ADDR'])." ";
	$body .= "User IP: ".$_SERVER['REMOTE_ADDR']." ";
	

	# SEND EMAIL
	
	mail($to,$subject,$body,"FROM: ".$from);
	
	# CHECK FOR FILE ERRORS
	
	if ($errorEmail != "") {
		
		$subject = $errorEmailSubject;
		
		$body = "";
		
		$numObj = sizeof($errorEmail);
		for ($i=0;$i<$numObj;$i++) {
			$body .= $errorEmail[$i]."\n";
		}
		
		$body .= "\nPosted From: ".$_SERVER['HTTP_REFERER']."\n";
		
		mail($to,$subject,$body,"FROM: ".$from);
	}
	
	# REDIRECT USERS
	
	if ($_POST["redirect"] != "")
		Header("location: ".$_POST["redirect"]);
	elseif ($redirect != "")
		Header("location: ".$redirect);
	else
		$submit_results = $langFormSubmitted;

}
?>

<? 

# DISPLAY HTML

if ($error != "" || $submit_results != "") {
?>

<?
if ($_POST["header"] != "" && file_exists($_POST["header"])) {
	include($_POST["header"]);
} elseif ($header_file != "" && file_exists($header_file)) {
	include($header_file);
} else {
?>
<html>
<head>
	<title><? if ($error != "") echo $langFormError; else echo $langFormResults; ?></title>
</head>

<body>
<br><br>
<?}?>

	<!-- Style -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet/less" type="text/css" href="../css/styles.less">
    <script src="../js/less.js"></script>


<table width="500" cellpadding="6" align="center" style="border: none;">
<tr>
	<td>
	<span style="font-family:arial; font-size:12pt">
	<? if ($error != "") { ?>
	<strong><?=$langSubmitError?></strong>
	<ul>
	<?
		$numObj = sizeof($error);
		for ($i=0;$i<$numObj;$i++) {
			echo "<li>".$error[$i]."\n";
		}
	?>
	</ul>
	<form><input type="button" class="btn btn-info" value="<?=$langGoBack?>" onClick="history.go(-1)"></form>
	<?
	} else {
		echo "<div align=\"center\">".$submit_results."</div>";
	}
	?>
	</span>
	</td>
</tr>
</table>

<?
if ($_POST["footer"] != "" && file_exists($_POST["footer"])) {
	include($_POST["footer"]);
} elseif ($footer_file != "" && file_exists($footer_file)) {
	include($footer_file);
} else {
?>
</body>
</html>
<? } ?>
<? } ?>