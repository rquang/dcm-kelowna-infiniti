<?php
//include("includes/session-check.php");
include("includes/top.php");
?>  
<div class="container_12">
<div class="grid_12">
  <form name="loginform" action="includes/form2email.php" method="post" id="lead_form"><!-- FORM STARTS -->   
    <input type="hidden" name="subject" value="Service Appointment - Kelowna Infiniti" />
    
    <!-- REQUIRED FIELDS START -->
    <input type="hidden" name="mileage_reqd" />
    <input type="hidden" name="first_name_reqd" />  
    <input type="hidden" name="last_name_reqd" />  
    <input type="hidden" name="email_reqd" />
    <input type="hidden" name="phone_reqd" />
    <input type="hidden" name="year_reqd" />
    <input type="hidden" name="make_reqd" />
    <input type="hidden" name="model_reqd" />
    
    <div id="lead_form_header">
      <h2>Service Appointment</h2>
      <div>Please enter your information below and a representative will contact you shortly.</div>
    </div>
    <div id="lead_status"></div>
    
    <!-- start of fields -->
    
    <fieldset><!-- DATE & TIME -->
      <legend><h3>Preferred Date &amp; Time</h3></legend>
      <ul>
        <li>
          <label class="col-xs-12 desc" for="appointment_date1">First Choice</label>
          <div class="one-line">
            <input class="date-picker left" name="appointment_date1" size="10" maxlength="10" type="text" value="" tabindex="14" />
            <select name="appointment_time1" tabindex="15" class="left appointment-time">
                  <option value="Morning" >Morning</option>
                  <option value="Noon" >Noon</option>
                  <option value="Afternoon" >Afternoon</option>
                </select>
          </div>
        </li>
        <li>
          <label class="col-xs-12 desc" for="appointment_date2">Second Choice</label>
          <div class="one-line">
            <input class="date-picker left" name="appointment_date2" size="10" maxlength="10" type="text" value="" tabindex="16" />
            <select name="appointment_time2" tabindex="17" class="left appointment-time">
                  <option value="Morning" >Morning</option>
                  <option value="Noon" >Noon</option>
                  <option value="Afternoon" >Afternoon</option>
                </select>
          </div>
        </li>
      </ul>
    </fieldset>
    
    <fieldset><!-- VEHICLE INFO -->
      <legend><h3>Vehicle Information</h3></legend>
      <ul>
        <li>
          <label class="col-xs-12 desc">Detail Information</label>
            <div>
              <span>
                <input name="year_other" type="text" value="" tabindex="18" />
                <label for="year_other">Vehicle Year</label>
              </span>
              <span>
                <input name="make_other" type="text" value="" tabindex="19" />
                <label for="make_other">Vehicle Make</label>
              </span>
              <span>
                <input name="model_other" type="text" value="" tabindex="20" />
                <label for="model_other">Vehicle Model</label>
              </span>
              <div class="clearfix"></div>
            </div>
        </li>     
      </ul>
    </fieldset>

    <fieldset><!-- CONTACT INFO -->
      <legend><h3>Contact Information</h3></legend>
      <ul>
        <li>
          <label class="col-xs-12 desc req" for="first_name">Contact Name</label>
          <span>
            <input name="first_name" type="text" value="" tabindex="1" />
            <label for="first_name">First Name</label>
          </span>
          <span>
            <input name="last_name" type="text" value="" tabindex="2" />
            <label for="last_name">Last Name</label>
          </span>
        </li>
        
        <li>
          <label class="col-xs-12 desc req" for="email">Contact Email</label>
          <div class="full">
            <input name="email" type="text" spellcheck="false" value="" maxlength="255" placeholder="you@example.com" tabindex="3" /> 
          </div>
        </li>
        
        <li>
          <label class="col-xs-12 desc" for="homephone">Contact Phone</label>
          <span>
            <input name="homephone" type="text" class="number" value="" maxlength="12" tabindex="4" placeholder="780-555-5555" />
            <label for="homephone">Home Phone</label>
          </span>
          
        </li>
      </ul>
    </fieldset>     
    
    <fieldset><!-- COMMENTS -->
      <legend><h3>Comments/Questions</h3></legend>
      <ul>
        <li>
          <label class="col-xs-12 desc" for="comments">Additional Information</label>
          <textarea class="textarea" name="comments" rows="4" cols="40" tabindex="24"></textarea>
        </li>
      </ul>
    </fieldset>
    
    <fieldset id="form-buttons"><!-- BUTTONS -->
      <input id="lead-form-submit" class="left btn btn-danger btn-large" type="submit" name="submit" value="Submit Information">
      <input type="reset" value="Clear Form" class="left" tabindex="26" />
    </fieldset>
    <!-- end of fields -->
  </form>
</div>
<!-- start number replacer -->

<?php include("includes/bottom.php"); ?>